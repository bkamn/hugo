---
title: Navodila
subtitle: Kako inštalirat ter začeti pisati
date: 2021-03-24
comments: false
---

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add posts
1. Push your changes 

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

### Creating posts

All posts are located in the `content/post/` folder. Simply copy an existing post, update the following data:

* post files have to follow the file name template: "YYYY-MM-DD-my-post.md" (e.g. 2021-03-23-my-post.md)
* post .md files contain header information at the top. Please change these accordingly when writing a new post.

        title: Postavitev custom updateDB okolja
        subtitle: Na kaj paziti pri postavitvi okolja za izvajanje .dbc skript
        date: 2021-03-24
        author: bkamn
        tags: ["maximo", "updateDB", "dbc"]
    
Now you can start writing and when you're done simply push your changes and they will be available online !

You can read up more on the markdown syntax used for writing posts here: [Markdown basics](https://www.markdownguide.org/basic-syntax/)



[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
