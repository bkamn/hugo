---
title: Postavitev custom updateDB okolja
subtitle: Na kaj paziti pri postavitvi okolja za izvajanje .dbc skript
date: 2021-03-24
author: bkamn
tags: ["maximo", "updateDB", "dbc"]
---

Troia kolikor sem spoznal uporablja definicijo "mx**TroiaCustomization.xml" za izvajanje vseh dbc skript. Za en projekt sem moral postaviti svoje okolje.

Koraki so bili naslednji:
1. copy-paste obstoječo troia definicijo (XML datoteka se mora nahajati v mapi: `/opt/IBM/SMP/maximo/applications/maximo/properties/product/`) 

2. Spremeni vrednosti XML polj:
    - *name*
    - *dbmaxvarname* (mora biti enkratno - ne sme že obstajati)
    - *dbversion* ter *lastdbversion* (postavi nazaj na V1000-01 oz. V1000-00). *dbversion* vsebuje cifro dbc skripte, ki se bo izvedla pri naslednjem zagonu updatedb.
    *lastdbversion* se uporabi pri validaciji z maximo bazo (in sicer z tabelo *MAXVARS*)
    - *dbscripts* (mapa v kateri bodo .dbc skripte za tvoje okolje. Pot je relativna in se začne na tej lokaciji `/opt/IBM/SMP/maximo/tools/maximo/en/troiaCustomization/ `)

3. Uploadaj .dbc skripte v mapo, ki si jo definiral v polju *dbscripts*. Ime dbc skript mora izgledati takole: `V1000_(zaporedna številka dbc skripte).dbc` (npr. V1000_01.dbc). 
    __Pazi da je podčrtaj '**_**' v imenu dbc file-a ter pomišljaj '**-**' v polju *dbversion* v tvojem XML-u !__

4. Ugasni maximo, spremeni working directory `cd /opt/IBM/SMP/maximo/tools/maximo/` ter zaženi skripto z `./updatedb.sh`

---

V primeru, da bojo napake v povezavi z *lastdbversion* sprobaj manualno insertat zapis v *MAXVARS* tabelo. (Zamenjaj 'TROIA-FIXEDASSSET' z vrednosti tvojga polja *dbmaxvarname*):
    
    INSERT INTO MAXVARS (VARNAME, VARVALUE, MAXVARSID) VALUES ('TROIA-FIXEDASSET', 'V1000-00', (SELECT max(MAXVARSID)+1 FROM MAXVARS));


Primer XML definicije okolja:

    <?xml version="1.0" encoding="UTF-8"?>
    <product>
        <name>Troia d.o.o. ESO Fixed Asset</name>
        <version>
            <major>1</major>
            <minor>0</minor>
            <modlevel>0</modlevel>
            <patch>0</patch>
            <build>20210318-1000</build>
        </version>
        <dbmaxvarname>TROIA-FIXEDASSET</dbmaxvarname>
        <dbscripts>troiaCustomizationFixedAsset</dbscripts>
        <dbversion>V1000-01</dbversion>
        <lastdbversion>V1000-00</lastdbversion>
        <extensions>
            
        </extensions>
    </product>