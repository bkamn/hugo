### Kaj je to ? 
Tukaj je baza znanja za maximo developerje. Vsebuje rešitve za pogoste probleme, ki jih maximo developerji srečujejo. Ta
stran se spotoma dopolnjuje s strani TROIA developerjev.

Za dodajanje člankov je potrebno imeti vzpostavljeno razvojno okolje pri sebi. Inštalacija je preprosta in članki se lahko pišejo v "markdown" sintaksi. 

**Navodila za inštalacijo ter pisanje so [tukaj](/page/howto/)**