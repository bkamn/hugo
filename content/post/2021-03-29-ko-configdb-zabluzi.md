---
title: Ko configDB zabluzi
subtitle: ....
date: 2021-03-29
author: bkamn
tags: ["maximo", "configDB", "konfiguracija baze", "workorder"]
---

ConfigDB bi naj izvezdel potrebne SQL stavke na podatkovno bazo, tako da bo stanje v aplikaciji "Konfiguracija baze" MATCHALO tistem v maximo bazi.
Ampak ta včasih zabluzi, crasha in te pusti z nedelujočim maxim-u ter malo oz. **nič** namigi kako rešit stvar.

### Prvi scenarij
Delaš na enem od EDP maximo okoljih ter dodaš par atributov na 2-3 objekte. Eden izmed njimi je objekt WORKORDER. WORKORDER tabela hrani veliko količino podatkov, se arhivira v tabelo A_WORKORDER in zahteva veliko procesiranja ko se izvede configDB na njej. 

Zgodi se nekaj takega:   

    [root@ismxepstg03 maximo]# ./configdb.sh
    Mon Feb 22 07:35:39 CET 2021 --- Starting ----
    BMXAA6806I - Reading the properties file maximo.properties.
    22 Feb 2021 07:35:40:556 [WARN] [] [] BMXAA6422W - The value for MAXPROP maximo.min.required.db.version could not be cached. The name in the properties file is not defined in the MaxProp table.
    22 Feb 2021 07:35:40:559 [WARN] [] [] BMXAA6422W - The value for MAXPROP mxe.mt.demo.extratenants could not be cached. The name in the properties file is not defined in the MaxProp table.
    BMXAA6818I - RestoreFromBackup started for schema MAXIMO, connected to database jdbc:db2://db2mx01.is.si:50002/mxdbepua Mon Feb 22 07:35:41 CET 2021
    BMXAA6818I - ConfigDB started for schema MAXIMO, connected to database jdbc:db2://db2mx01.is.si:50002/mxdbepua Mon Feb 22 07:35:41 CET 2021
    BMXAA0364I - Getting metadata Mon Feb 22 07:35:42 CET 2021
    BMXAA0349I - Configuring tables Mon Feb 22 07:35:42 CET 2021
    BMXAA0345I - Altering table A_WORKORDER. Mon Feb 22 07:35:42 CET 2021
    BMXAA0345I - Altering table DOCINFO. Mon Feb 22 07:35:42 CET 2021
    BMXAA0345I - Altering table WORKORDER. Mon Feb 22 07:35:42 CET 2021
    com.ibm.db2.jcc.am.SqlTransactionRollbackException: The current transaction has been rolled back because of a deadlock or timeout.  Reason code "68".. SQLCODE=-911, SQLSTATE=40001, DRIVER=3.69.71
            at com.ibm.db2.jcc.am.gd.a(gd.java:736)
            ...
            at psdi.configure.ConfigDB.main(ConfigDB.java:4614)
    The current transaction has been rolled back because of a deadlock or timeout.  Reason code "68".. SQLCODE=-911, SQLSTATE=40001, DRIVER=3.69.71 Mon Feb 22 07:40:43 CET 2021
    BMXAA6819I - ConfigDB completed with errors. Mon Feb 22 07:40:43 CET 2021
    BMXAA6820I - RestoreFromBackup completed without errors. Mon Feb 22 07:40:43 CET 2021

**Timeout oz. deadlock** ! Si misliš okej, bom pognal še 1x, mogoče pa bo zdej.

    ...
    BMXAA6818I - ConfigDB started for schema MAXIMO, connected to database jdbc:db2://db2mx01.is.si:50002/mxdbepua Mon Feb 22 07:46:46 CET 2021
    BMXAA0484E - Column cannot be added because it already exists in the database: A_WORKORDER.MXEOMFERROR Mon Feb 22 07:46:46 CET 2021
    BMXAA6820I - ConfigDB completed without errors. Mon Feb 22 07:46:46 CET 2021
    BMXAA6820I - RestoreFromBackup completed without errors. Mon Feb 22 07:46:46 CET 2021
 
 
*A_WORKORDER.MXEOMFERROR* že obstaja ?  Menda je prepovedano, ampak ne vidim druge: 
`ALTER TABLE A_WORKORDER DROP MXEOMFERROR;`

...  

in potem spet timeout


OKEJ, bom porazdelil configdb (cdb). Bom prvo dodal atribut na WORKORDER, izvedo cdb, potem pa z drugim cdb-jem dodal atribut na DOCINFO. Prižgeš nazaj serverje, da bi šel v "Konfiguracijo baze" - **maximo pa ne dela !**

Problem je v vrednosti polja "CONFIGURING" v tabeli MAXVARS. Ko je namreč ta nastavljen na "1", bi se naj izvajal še cdb. A le ta je crash-al in vrednosti ni nastavil nazaj.  

Po tem bo maximo spet deloval:  
`UPDATE MAXVARS SET VARVALUE=0 WHERE VARNAME = 'CONFIGURING'`


Kakorkoli že, po nekaj več poskušanja je stvar šla skoz in sonce se je spet prikazalo.

#### Kar tudi zna pomagati
- [odstraniti "-r" iz configdb skripte](https://www.ibm.com/support/knowledgecenter/en/SSLKT6_7.6.0.5/com.ibm.mbs.doc/configur/t_config_command_line_mode.html). A ne pozabit nato izvesti še *./restorefrombackup.sh*
